"""
MateBot API schema definitions
"""

from .bases import *
from .config import *
from .errors import *
from .events import *
from .extra import *
from .groups import *
