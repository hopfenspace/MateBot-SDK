"""
Special schemas for the configuration file and its properties
"""

import pydantic


class ServerSettings(pydantic.BaseModel):
    min_refund_approves: pydantic.PositiveInt
    min_refund_disapproves: pydantic.PositiveInt
    min_membership_approves: pydantic.PositiveInt
    min_membership_disapproves: pydantic.PositiveInt
    max_parallel_debtors: pydantic.PositiveInt
    max_simultaneous_consumption: pydantic.conint(gt=2)
    max_transaction_amount: pydantic.conint(gt=100)
