class MateBotSDKException(Exception):
    """
    Base class for exceptions specific to the MateBot SDK
    """

    def __init__(self, message: str):
        self.message = message


class APIConnectionException(MateBotSDKException):
    """
    Exception class indicating failed API requests because of problems reaching the server

    There's little the client can do about such a problem except for checking the
    internet connectivity and API server reachability. However, this error class
    also provides three fields: `exc`, `message` and `details`. The first field holds
    a reference to the underlying HTTP library exception, the second a user-friendly
    error message and the third field the combined error message of both classes. This
    class was built to wrap around various HTTP libraries and provide the `message`.
    """

    def __init__(self, exc: Exception, message: str, details: str):
        self.exc = exc
        self.message = message
        self.details = details


class APIException(MateBotSDKException):
    """
    Exception class indicating failed API requests

    This class provides the `status`, `message` and `details` fields. The first
    indicates the HTTP status code of the response, the second an optional message
    (which might be shown to end-users or not; it might not contain information a
    user wants to mess around with) and the third a string, probably containing
    JSON-encoded data from the API server (definitively not suited for end-users).
    """

    def __init__(self, status: int, message: str, details: str):
        self.status = status
        self.message = message
        self.details = details

    def __str__(self) -> str:
        return f"{self.message} ({self.status})"

    def __repr__(self) -> str:
        return str({"status": self.status, "message": self.message, "details": self.details})
