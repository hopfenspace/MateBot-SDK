"""
MateBot SDK base module
"""

import enum
import types
import asyncio
import inspect
import logging
import collections
from functools import singledispatch, wraps
from typing import Any, Awaitable, Callable, Dict, Generator, List, Optional, Tuple

from . import schemas


class HTTPMethod(enum.Enum):
    GET = "GET"
    POST = "POST"
    DELETE = "DELETE"


class PermissionLevel(enum.IntEnum):
    ANYONE = 0
    ANY_ACTIVE = 1
    ANY_WITH_VOUCHER = 2
    ANY_INTERNAL = 3
    ANY_WITH_PERMISSION = 4
    NOBODY = 5


@singledispatch
def synced(obj: Any):
    raise TypeError(f"Called with unsupported argument {obj} of type {type(obj)}")


@synced.register(asyncio.Future)
@synced.register(types.GeneratorType)
def _(co: Generator) -> Any:
    if not inspect.isawaitable(co):
        raise TypeError(f"Called with unsupported argument {co} of type {type(co)}")
    return asyncio.get_event_loop().run_until_complete(co)


@synced.register(types.MethodType)
@synced.register(types.FunctionType)
def _(f: Callable) -> Callable:
    if not asyncio.iscoroutinefunction(f):
        raise TypeError(f"Called with unsupported argument {f} of type {type(f)}")

    @wraps(f)
    def run(*args, **kwargs):
        return asyncio.get_event_loop().run_until_complete(f(*args, **kwargs))
    return run


CALLBACK_TYPE = Callable[[schemas.Event], Optional[Awaitable[None]]]
STORAGE_TYPE = Dict[schemas.EventType, List[Tuple[CALLBACK_TYPE, tuple, dict]]]


class BaseCallbackDispatcher:
    """
    Abstract base class for callback dispatcher handlers

    A real implementation should create a subclass and implement the
    ``run_callback`` function. This function should execute the specified
    callback in a thread-safe manner. It may or may not execute in the
    environment where async routines can be used, since this depends
    on the actual usage in the client application. The ``run_callback``
    method gets the callback function (or coroutine) as well as the
    dispatched event method and any args and kwargs defined in ``register``.
    Additionally, access to any extra arguments defined while creating
    the class is possible via the ``xargs`` attribute. Take care
    of thread-safety when accessing this attribute.

    Additionally, the client application needs to provide the web
    server implementation on its own. Dispatching events is then
    easily possible via the ``dispatch`` method.
    """

    def __init__(self, *xargs, logger: Optional[logging.Logger] = None):
        self.xargs: tuple = xargs
        self.logger = logger
        self._storage: STORAGE_TYPE = collections.defaultdict(list)

    def register_for(self, event_type: schemas.EventType, *args, **kwargs) -> Callable[[CALLBACK_TYPE], CALLBACK_TYPE]:
        """
        Get a decorator to register a new callback function or coroutine to be executed for a given event

        :param event_type: type of event the callback function should be registered for
        :param args: any positional arguments given to the callback function
        :param kwargs: any keyword arguments given to the callback function
        :return: a decorator function that should be used to annotate the callback function
        """

        def decorator(func):
            self.register(event_type, func, *args, **kwargs)
            return func
        return decorator

    def register(self, event_type: schemas.EventType, func: CALLBACK_TYPE, *args, **kwargs):
        """
        Register a new callback function or coroutine to be executed for a given event

        :param event_type: type of event the callback function should be registered for
        :param func: callback function or coroutine
        :param args: any positional arguments given to the callback function
        :param kwargs: any keyword arguments given to the callback function
        """

        if not isinstance(event_type, schemas.EventType):
            raise TypeError(f"Invalid event type {type(event_type)}, expected enum {schemas.EventType!r}")
        self._storage[event_type].append((func, args, kwargs))

    def dispatch(self, events: schemas.EventsNotification):
        """
        Dispatch an incoming events notification to trigger all registered callback handlers

        :param events: incoming notification about a collection of events
        """

        for event in events.events:
            for target, args, kwargs in self._storage.get(event.event, []):
                try:
                    self.run_callback(target, event, target, *args, **kwargs)
                except Exception as exc:
                    self.logger and self.logger.exception(
                        f"{type(exc).__name__} in callback handler {target} for event {event!r} with {args}, {kwargs}"
                    )

    def run_callback(self, func: CALLBACK_TYPE, event: schemas.Event, *args, **kwargs):
        raise NotImplementedError


class BaseSDK:
    """
    Base SDK providing functions and methods to ease the implementation of actual clients

    Any functionality found here can be used by both the sync and async SDK classes.

    Note that the `setup` method should be called after
    initialization to complete some necessary steps.
    """

    def __init__(
            self,
            base_url: str,
            app_name: str,
            logger: logging.Logger = None
    ):
        self.base_url = base_url and (base_url[:-1] if base_url.endswith("/") else base_url)
        self.app_name = app_name
        self._logger = logger or logging.getLogger(__name__)
        self._app_id = None

    def setup(self):
        raise NotImplementedError

    def close(self):
        raise NotImplementedError
