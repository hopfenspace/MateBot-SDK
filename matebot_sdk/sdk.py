"""
MateBot backend API wrapper to easily query and use the remote services
"""

import ssl
import logging
import urllib.parse
from typing import List, Optional, Tuple, Type, TypeVar, Union

import httpx
import pydantic

from . import auth, schemas
from .base import BaseSDK, HTTPMethod
from .exceptions import APIException, APIConnectionException, MateBotSDKException


Model = TypeVar("Model")
UserRefType = Union[schemas.User, int, str]
TIMEOUT_DEFAULT = 3.0


class AsyncSDK(BaseSDK):
    """
    Async MateBot core API wrapper class
    """

    def __init__(
            self,
            base_url: str,
            app_name: str,
            password: str,
            callback: Union[str, Tuple[str], Tuple[str, str], None] = None,
            logger: logging.Logger = None,
            verify: Union[str, bool, ssl.SSLContext] = True,
            user_agent: str = None,
            timeout: Optional[float] = TIMEOUT_DEFAULT,
            scope: str = "",
            client_id: str = "",
            client_secret: str = ""
    ):
        super().__init__(base_url, app_name, logger)
        self._callback = None
        self._callback_secret = None
        if callback is not None:
            if isinstance(callback, str):
                self._callback = callback
            elif isinstance(callback, tuple):
                if len(callback) == 1:
                    self._callback = callback[0]
                elif len(callback) == 2:
                    self._callback, self._callback_secret = callback
                else:
                    raise ValueError("Expecting exactly one or two arguments for callback tuple")

        self._client = httpx.AsyncClient(
            auth=auth.APIAuthentication(
                self.base_url + "/v1/login",
                username=app_name,
                password=password,
                logger=logger,
                scope=scope,
                client_id=client_id,
                client_secret=client_secret
            ),
            base_url=base_url,
            verify=verify,
            headers={"User-Agent": user_agent} if user_agent else None,
            timeout=timeout
        )

    ########################
    # Start & stop methods #
    ########################

    async def setup(self):
        self._logger.debug("Setting up...")
        if not await self.is_server_healthy():
            self._logger.warning("Server reported unhealthy state")
        await self.application
        await self.delete_all_callbacks(self._app_id)
        if self._callback:
            await self.create_callback(
                url=self._callback,
                application=self._app_id,
                shared_secret=self._callback_secret
            )

    async def close(self):
        self._logger.debug("Closing...")
        await self.delete_all_callbacks(self._app_id)
        await self._client.aclose()

    ##################################
    # Methods for actual network I/O #
    ##################################

    async def _request(
            self,
            method: HTTPMethod,
            endpoint: str,
            logger: logging.Logger,
            success_codes: List[int],
            model: Union[None, Type[Model], List[Type[Model]]],
            data: Union[dict, pydantic.BaseModel, None] = None
    ) -> Model:
        logger.debug(f"Trying '{method.value} {endpoint}' (expecting {model})...")

        try:
            if data is None:
                response = await self._client.request(method.value, endpoint)
            elif isinstance(data, pydantic.BaseModel):
                response = await self._client.request(method.value, endpoint, json=data.dict())
            else:
                response = await self._client.request(method.value, endpoint, json=data)
        except httpx.RequestError as exc:
            logger.exception(f"Failed to successfully contact the API server: {type(exc).__name__}")
            details = f"Connection failure. {type(exc).__name__}: {str(exc)}"
            raise APIConnectionException(
                exc=exc,
                message="The API server is not reachable. Please ensure proper network connectivity.",
                details=details
            ) from exc
        except:
            logger.exception(f"Exception on '{method.value} {endpoint}'")
            raise

        status = response.status_code
        if status in success_codes:
            if model is None:
                return None
            data = response.json()
            try:
                if isinstance(model, list):
                    if len(model) != 1:
                        raise ValueError("Expected exactly one element in the list of models")
                    if not isinstance(data, list):
                        raise ValueError("Expected result to be a list, but it isn't")
                    return [model[0](**element) for element in data]
                else:
                    model: Type[Model]
                    return model(**data)
            except (TypeError, ValueError) as exc:
                logger.error(f"Failed to convert server response via {model!r}: {data!r}")
                raise APIConnectionException(
                    exc=exc,
                    message="The API server sent a response which couldn't be processed. Your action wasn't performed.",
                    details=response.text
                ) from exc

        try:
            data = response.json()
        except (TypeError, ValueError) as exc:
            logger.error(f"Server failed to deliver valid JSON (decoding error): {response.text}")
            raise APIConnectionException(
                exc=exc,
                message="The API server sent a response which wasn't understood. Your action wasn't performed.",
                details=response.text
            ) from exc

        try:
            error = schemas.APIError(**data)
            raise APIException(status=status, message=error.message, details=error.details)
        except ValueError as exc:
            logger.error(f"Server failed to deliver correct error response (no valid JSON schema): {response.text}")
            raise APIConnectionException(
                exc=exc,
                message="The API server sent a response which wasn't understood. Your action wasn't performed.",
                details=response.text
            ) from exc

    async def _get(
            self,
            endpoint: str,
            model: Union[Type[Model], List[Type[Model]]],
            logger: logging.Logger = None,
            **kwargs
    ) -> Union[Model, List[Model]]:
        return await self._request(
            HTTPMethod.GET, self._make_query(endpoint, **kwargs), logger or self._logger, [200], model
        )

    async def _post(
            self,
            endpoint: str,
            model: Type[Model],
            data: Union[dict, pydantic.BaseModel, None] = None,
            logger: logging.Logger = None
    ) -> Model:
        return await self._request(HTTPMethod.POST, endpoint, logger or self._logger, [200, 201], model, data)

    async def _delete(
            self,
            endpoint: str,
            data: Union[dict, pydantic.BaseModel, None] = None,
            logger: logging.Logger = None
    ) -> None:
        return await self._request(HTTPMethod.DELETE, endpoint, logger or self._logger, [204], None, data)

    @staticmethod
    def _make_query(path: str, **kwargs) -> str:
        args = urllib.parse.urlencode([
            (k, v) for k, v in kwargs.items()
            if v is not None and v != "" and any(isinstance(v, t) for t in (int, bool, str, float))
        ])
        return path + (args and "?" + args)

    async def _get1(self, path: str, model: Type[Model], **kwargs) -> Model:
        results = await self._get(path, [model], **kwargs)
        try:
            return results[0]
        except IndexError:
            self._logger.warning(f"Call to {path!r} with args {kwargs} didn't produce any results")
            raise APIException(status=200, message=f"The requested {model.__name__} was not found.", details="[]")

    ###################################################
    # Collection of async properties for easier usage #
    ###################################################

    @property
    def app_id(self) -> int:
        """
        Current application identifier
        """

        return self._app_id

    @property
    async def application(self) -> schemas.Application:
        """
        Current application property
        """

        if self._app_id is not None:
            return await self._get1("/v1/applications", schemas.Application, id=self._app_id)
        app = await self._get1("/v1/applications", schemas.Application, name=self.app_name)
        self._app_id = app.id
        return app

    @property
    async def users(self) -> List[schemas.User]:
        """
        All users which have an alias in the current application
        """

        if self._app_id is None:
            await self.application
        return await self._get("/v1/users", [schemas.User], alias_application_id=self._app_id)

    @property
    async def community(self) -> schemas.User:
        """
        Community user property
        """

        return await self.get_community_user()

    ############################
    # Generic server endpoints #
    ############################

    async def get_server_settings(self) -> schemas.ServerSettings:
        return await self._get("/v1/settings", schemas.ServerSettings)

    async def is_server_healthy(self) -> bool:
        try:
            await self._get("/v1/health", pydantic.BaseModel)
        except (APIConnectionException, RuntimeError):
            return False
        return True

    ########################
    # Search functionality #
    ########################

    async def get_aliases(
            self,
            id: Optional[int] = None,  # noqa
            user_id: Optional[int] = None,
            application_id: Optional[int] = None,
            username: Optional[str] = None,
            confirmed: Optional[bool] = None,
            limit: Optional[int] = None,
            page: Optional[int] = None,
            descending: Optional[bool] = None
    ) -> List[schemas.Alias]:
        return await self._get(
            "/v1/aliases",
            [schemas.Alias],
            id=id,
            user_id=user_id,
            application_id=application_id,
            username=username,
            confirmed=confirmed,
            limit=limit,
            page=page,
            descending=descending
        )

    async def get_applications(
            self,
            id: Optional[int] = None,  # noqa
            name: str = None,
            callback_id: int = None,
            limit: Optional[int] = None,
            page: Optional[int] = None,
            descending: Optional[bool] = None
    ) -> List[schemas.Application]:
        return await self._get(
            "/v1/applications",
            [schemas.Application],
            id=id,
            name=name,
            callback_id=callback_id,
            limit=limit,
            page=page,
            descending=descending
        )

    async def get_callbacks(
            self,
            id: Optional[int] = None,  # noqa
            url: Optional[str] = None,
            application_id: Optional[int] = None,
            limit: Optional[int] = None,
            page: Optional[int] = None,
            descending: Optional[bool] = None
    ) -> List[schemas.Callback]:
        return await self._get(
            "/v1/callbacks",
            [schemas.Callback],
            id=id,
            url=url,
            application_id=application_id,
            limit=limit,
            page=page,
            descending=descending
        )

    async def get_communisms(
            self,
            id: Optional[int] = None,  # noqa
            active: Optional[bool] = None,
            amount: Optional[int] = None,
            description: Optional[str] = None,
            creator_id: Optional[int] = None,
            participant_id: Optional[int] = None,
            total_participants: Optional[int] = None,
            unique_participants: Optional[int] = None,
            limit: Optional[int] = None,
            page: Optional[int] = None,
            descending: Optional[bool] = None
    ) -> List[schemas.Communism]:
        return await self._get(
            "/v1/communisms",
            [schemas.Communism],
            id=id,
            active=active,
            amount=amount,
            description=description,
            creator_id=creator_id,
            participant_id=participant_id,
            total_participants=total_participants,
            unique_participants=unique_participants,
            limit=limit,
            page=page,
            descending=descending
        )

    async def get_consumables(
            self,
            name: Optional[str] = None,
            description: Optional[str] = None,
            price: Optional[int] = None
    ) -> List[schemas.Consumable]:
        return await self._get(
            "/v1/consumables",
            [schemas.Consumable],
            name=name,
            description=description,
            price=price
        )

    async def get_polls(
            self,
            id: Optional[int] = None,  # noqa
            active: Optional[bool] = None,
            accepted: Optional[bool] = None,
            user_id: Optional[int] = None,
            ballot_id: Optional[int] = None,
            limit: Optional[int] = None,
            page: Optional[int] = None,
            descending: Optional[bool] = None
    ) -> List[schemas.Poll]:
        return await self._get(
            "/v1/polls",
            [schemas.Poll],
            id=id,
            active=active,
            accepted=accepted,
            user_id=user_id,
            ballot_id=ballot_id,
            limit=limit,
            page=page,
            descending=descending
        )

    async def get_refunds(
            self,
            id: Optional[int] = None,  # noqa
            amount: Optional[int] = None,
            description: Optional[str] = None,
            active: Optional[bool] = None,
            creator_id: Optional[int] = None,
            ballot_id: Optional[int] = None,
            transaction_id: Optional[int] = None,
            limit: Optional[int] = None,
            page: Optional[int] = None,
            descending: Optional[bool] = None
    ) -> List[schemas.Refund]:
        return await self._get(
            "/v1/refunds",
            [schemas.Refund],
            id=id,
            amount=amount,
            description=description,
            active=active,
            creator_id=creator_id,
            ballot_id=ballot_id,
            transaction_id=transaction_id,
            limit=limit,
            page=page,
            descending=descending
        )

    async def get_transactions(
            self,
            id: Optional[int] = None,  # noqa
            sender_id: Optional[int] = None,
            receiver_id: Optional[int] = None,
            member_id: Optional[int] = None,
            amount: Optional[int] = None,
            reason: Optional[str] = None,
            has_multi_transaction: Optional[bool] = None,
            multi_transaction_id: Optional[int] = None,
            limit: Optional[int] = None,
            page: Optional[int] = None,
            descending: Optional[bool] = None
    ) -> List[schemas.Transaction]:
        return await self._get(
            "/v1/transactions",
            [schemas.Transaction],
            id=id,
            sender_id=sender_id,
            receiver_id=receiver_id,
            member_id=member_id,
            amount=amount,
            reason=reason,
            has_multi_transaction=has_multi_transaction,
            multi_transaction_id=multi_transaction_id,
            limit=limit,
            page=page,
            descending=descending
        )

    async def get_users(
            self,
            id: Optional[int] = None,  # noqa
            name: Optional[str] = None,
            community: Optional[bool] = None,
            permission: Optional[bool] = None,
            active: Optional[bool] = None,
            external: Optional[bool] = None,
            voucher_id: Optional[int] = None,
            alias_id: Optional[int] = None,
            alias_username: Optional[str] = None,
            alias_confirmed: Optional[bool] = None,
            alias_application_id: Optional[int] = None,
            limit: Optional[int] = None,
            page: Optional[int] = None,
            descending: Optional[bool] = None
    ) -> List[schemas.User]:
        return await self._get(
            "/v1/users",
            [schemas.User],
            id=id,
            community=community,
            name=name,
            permission=permission,
            active=active,
            external=external,
            voucher_id=voucher_id,
            alias_id=alias_id,
            alias_username=alias_username,
            alias_confirmed=alias_confirmed,
            alias_application_id=alias_application_id,
            limit=limit,
            page=page,
            descending=descending
        )

    async def get_votes(
            self,
            id: Optional[int] = None,  # noqa
            vote: Optional[bool] = None,
            ballot_id: Optional[int] = None,
            user_id: Optional[int] = None,
            vote_for_poll: Optional[bool] = None,
            vote_for_refund: Optional[bool] = None,
            limit: Optional[int] = None,
            page: Optional[int] = None,
            descending: Optional[bool] = None
    ) -> List[schemas.Vote]:
        return await self._get(
            "/v1/votes",
            [schemas.Vote],
            id=id,
            vote=vote,
            ballot_id=ballot_id,
            user_id=user_id,
            vote_for_poll=vote_for_poll,
            vote_for_refund=vote_for_refund,
            limit=limit,
            page=page,
            descending=descending
        )

    #################
    # User handling #
    #################

    async def get_user(self, user: UserRefType) -> schemas.User:
        if isinstance(user, schemas.User):
            return user
        try:
            if isinstance(user, int):
                return (await self.get_users(active=True, id=user))[0]
            elif isinstance(user, str):
                return (await self.get_users(active=True, name=user))[0]
            raise TypeError(f"Unexpected user type {type(user)!r}")
        except IndexError:
            raise MateBotSDKException(f"Unknown user {user!r}")

    async def get_community_user(self) -> schemas.User:
        return (await self.get_users(community=True))[0]

    async def find_sponsors(self, issuer: UserRefType, count: int = 1) -> List[schemas.User]:
        issuer = await self.get_user(issuer)
        if issuer.privilege < schemas.PrivilegeLevel.INTERNAL:
            raise MateBotSDKException("You are not allowed to use this feature.")
        if count < 1:
            raise ValueError("Count must be greater or equal to 1")
        return sorted(
            filter(lambda u: u.balance < 0, await self.get_users(active=True, community=False)),
            key=lambda u: u.balance
        )[:count]

    async def get_users_by_alias(
            self,
            alias: Union[int, str, schemas.Alias],
            confirmed: Optional[bool] = True,
            app: Union[int, schemas.Application] = None,
            active: Optional[bool] = True
    ) -> List[schemas.User]:
        app = app or self._app_id or await self.application
        if isinstance(app, schemas.Application):
            app = app.id
        if isinstance(alias, schemas.Alias):
            alias = alias.id
        if isinstance(alias, int):
            return await self.get_users(active=active, alias_id=alias)
        return await self.get_users(
            active=active,
            alias_username=alias,
            alias_confirmed=confirmed,
            alias_application_id=app
        )

    async def create_plain_user(self, username: str) -> schemas.User:
        user = schemas.UserCreation(name=username)
        return await self._post("/v1/users", schemas.User, data=user)

    async def create_app_user(
            self,
            username: str,
            alias_username: str,
            alias_confirmed: bool = True
    ) -> schemas.User:
        user = await self.create_plain_user(username)
        await self.create_alias(user, alias_username, alias_confirmed)
        return (await self.get_users(id=user.id))[0]

    async def vouch_for(
            self,
            debtor: UserRefType,
            voucher: Optional[UserRefType],
            issuer: UserRefType
    ) -> schemas.VoucherUpdateResponse:
        if isinstance(debtor, schemas.User):
            debtor = debtor.id
        if isinstance(voucher, schemas.User):
            voucher = voucher.id
        if isinstance(issuer, schemas.User):
            issuer = issuer.id
        request = schemas.VoucherUpdateRequest(debtor=debtor, issuer=issuer, voucher=voucher)
        return await self._post("/v1/users/setVoucher", schemas.VoucherUpdateResponse, request)

    async def vouch_stop(self, debtor: UserRefType, issuer: UserRefType) -> schemas.VoucherUpdateResponse:
        return await self.vouch_for(debtor, None, issuer)

    async def _drop_privilege(self, endpoint: str, user: UserRefType, issuer: UserRefType) -> schemas.User:
        user = await self.get_user(user)
        issuer = await self.get_user(issuer)
        drop = schemas.UserPrivilegeDrop(user=user, issuer=issuer)
        return await self._post(endpoint, schemas.User, drop)

    async def drop_internal_privilege(self, user: UserRefType, issuer: UserRefType) -> schemas.User:
        return await self._drop_privilege("/v1/users/dropInternal", user, issuer)

    async def drop_permission_privilege(self, user: UserRefType, issuer: UserRefType) -> schemas.User:
        return await self._drop_privilege("/v1/users/dropPermission", user, issuer)

    async def set_username(self, username: str, issuer: UserRefType) -> schemas.User:
        if isinstance(issuer, schemas.User):
            issuer = issuer.id
        request = schemas.UsernameUpdateRequest(issuer=issuer, name=username)
        return await self._post("/v1/users/setName", schemas.User, request)

    async def delete_user(self, user: Union[int, schemas.User], issuer: UserRefType) -> schemas.User:
        if isinstance(user, schemas.User):
            user = user.id
        if isinstance(issuer, schemas.User):
            issuer = issuer.id
        return await self._post("/v1/users/delete", schemas.User, schemas.IssuerIdBody(id=user, issuer=issuer))

    ##################
    # Alias handling #
    ##################

    async def create_alias(
            self,
            user: Union[int, schemas.User],
            username: str,
            confirmed: bool = False,
            application: Optional[schemas.Application] = None
    ) -> schemas.Alias:
        if isinstance(user, schemas.User):
            user = user.id
        app_id = (application is not None and application.id) or self._app_id or (await self.application).id
        request = schemas.AliasCreation(username=username, confirmed=confirmed, application_id=app_id, user_id=user)
        return await self._post("/v1/aliases", schemas.Alias, request)

    async def _modify_alias(
            self,
            alias: Union[int, schemas.Alias],
            issuer: UserRefType,
            path: str,
            t: Type[Model]
    ) -> Model:
        if isinstance(alias, schemas.Alias):
            alias = alias.id
        if isinstance(issuer, schemas.User):
            issuer = issuer.id
        request = schemas.IssuerIdBody(id=alias, issuer=issuer)
        return await self._post(path, t, request)

    async def confirm_alias(self, alias: Union[int, schemas.Alias], issuer: UserRefType) -> schemas.Alias:
        return await self._modify_alias(alias, issuer, "/v1/aliases/confirm", schemas.Alias)

    async def delete_alias(self, alias: Union[int, schemas.Alias], issuer: UserRefType) -> schemas.AliasDeletion:
        return await self._modify_alias(alias, issuer, "/v1/aliases/delete", schemas.AliasDeletion)

    ########################
    # Transaction handling #
    ########################

    async def create_transaction(
            self,
            sender: UserRefType,
            receiver: UserRefType,
            amount: int,
            reason: str
    ) -> schemas.Transaction:
        if isinstance(sender, schemas.User):
            sender = sender.id
        if isinstance(receiver, schemas.User):
            receiver = receiver.id
        request = schemas.TransactionCreation(sender=sender, receiver=receiver, amount=amount, reason=reason)
        return await self._post("/v1/transactions/send", schemas.Transaction, request)

    async def create_consumption(
            self,
            consumable: Union[str, schemas.Consumable],
            amount: int,
            user: UserRefType
    ) -> schemas.Transaction:
        if isinstance(consumable, schemas.Consumable):
            consumable = consumable.name
        if isinstance(user, schemas.User):
            user = user.id
        request = schemas.Consumption(user=user, amount=amount, consumable=consumable)
        return await self._post("/v1/transactions/consume", schemas.Transaction, request)

    ######################
    # Communism handling #
    ######################

    async def create_communism(
            self,
            creator: UserRefType,
            amount: int,
            description: str
    ) -> schemas.Communism:
        if isinstance(creator, schemas.User):
            creator = creator.id
        request = schemas.CommunismCreation(amount=amount, creator=creator, description=description)
        return await self._post("/v1/communisms", schemas.Communism, request)

    async def abort_communism(self, communism: Union[int, schemas.Communism], issuer: UserRefType) -> schemas.Communism:
        if isinstance(communism, schemas.Communism):
            communism = communism.id
        if isinstance(issuer, schemas.User):
            issuer = issuer.id
        request = schemas.IssuerIdBody(id=communism, issuer=issuer)
        return await self._post("/v1/communisms/abort", schemas.Communism, request)

    async def close_communism(self, communism: Union[int, schemas.Communism], issuer: UserRefType) -> schemas.Communism:
        if isinstance(communism, schemas.Communism):
            communism = communism.id
        if isinstance(issuer, schemas.User):
            issuer = issuer.id
        request = schemas.IssuerIdBody(id=communism, issuer=issuer)
        return await self._post("/v1/communisms/close", schemas.Communism, request)

    async def _mod_communism_participation(
            self,
            communism: Union[int, schemas.Communism],
            user: UserRefType,
            diff: int,
            endpoint: str
    ) -> schemas.Communism:
        if isinstance(communism, schemas.Communism):
            communism = communism.id
        if isinstance(user, schemas.User):
            user = user.id
        request = schemas.CommunismParticipationUpdate(id=communism, user=user)
        if diff <= 0:
            raise ValueError("Communism participation difference can't be zero or negative")
        result = None
        for i in range(diff):
            result = await self._post(endpoint, schemas.Communism, request)
        return result

    async def increase_communism_participation(
            self,
            communism: Union[int, schemas.Communism],
            user: UserRefType,
            count: int = 1
    ) -> schemas.Communism:
        return await self._mod_communism_participation(communism, user, count, "/v1/communisms/increaseParticipation")

    async def decrease_communism_participation(
            self,
            communism: Union[int, schemas.Communism],
            user: UserRefType,
            count: int = 1
    ) -> schemas.Communism:
        return await self._mod_communism_participation(communism, user, count, "/v1/communisms/decreaseParticipation")

    ############################
    # Membership poll handling #
    ############################

    async def create_poll_get_internal(self, user: UserRefType, issuer: UserRefType) -> schemas.Poll:
        return await self.create_poll(user, issuer, schemas.PollVariant.GET_INTERNAL)

    async def create_poll_get_permission(self, user: UserRefType, issuer: UserRefType) -> schemas.Poll:
        return await self.create_poll(user, issuer, schemas.PollVariant.GET_PERMISSION)

    async def create_poll_loose_internal(self, user: UserRefType, issuer: UserRefType) -> schemas.Poll:
        return await self.create_poll(user, issuer, schemas.PollVariant.LOOSE_INTERNAL)

    async def create_poll_loose_permission(self, user: UserRefType, issuer: UserRefType) -> schemas.Poll:
        return await self.create_poll(user, issuer, schemas.PollVariant.LOOSE_PERMISSION)

    async def create_poll(self, user: UserRefType, issuer: UserRefType, variant: schemas.PollVariant) -> schemas.Poll:
        if isinstance(user, schemas.User):
            user = user.id
        if isinstance(issuer, schemas.User):
            issuer = issuer.id
        if not isinstance(variant, schemas.PollVariant):
            raise TypeError(f"Expected PollVariant, found {type(variant)!r}")
        request = schemas.PollCreation(user=user, issuer=issuer, variant=variant)
        return await self._post("/v1/polls", schemas.Poll, request)

    async def _vote_in_poll(
            self,
            poll: Union[int, schemas.Poll],
            user: UserRefType,
            vote: bool
    ) -> schemas.PollVoteResponse:
        if isinstance(user, schemas.User):
            user = user.id
        if isinstance(poll, int):
            poll = await self._get1("/v1/polls", schemas.Poll, id=poll)
        request = schemas.VoteCreation(vote=vote, user=user, ballot_id=poll.ballot_id)
        return await self._post("/v1/polls/vote", schemas.PollVoteResponse, request)

    async def approve_poll(
            self,
            poll: Union[int, schemas.Poll],
            user: UserRefType
    ) -> schemas.PollVoteResponse:
        return await self._vote_in_poll(poll, user, True)

    async def disapprove_poll(
            self,
            poll: Union[int, schemas.Poll],
            user: UserRefType
    ) -> schemas.PollVoteResponse:
        return await self._vote_in_poll(poll, user, False)

    async def abort_poll(self, poll: Union[int, schemas.Poll], issuer: UserRefType) -> schemas.Poll:
        if isinstance(poll, schemas.Poll):
            poll = poll.id
        if isinstance(issuer, schemas.User):
            issuer = issuer.id
        request = schemas.IssuerIdBody(id=poll, issuer=issuer)
        return await self._post("/v1/polls/abort", schemas.Poll, request)

    ###################
    # Refund handling #
    ###################

    async def create_refund(
            self,
            creator: UserRefType,
            amount: int,
            description: str
    ) -> schemas.Refund:
        if isinstance(creator, schemas.User):
            creator = creator.id
        request = schemas.RefundCreation(creator=creator, amount=amount, description=description)
        return await self._post("/v1/refunds", schemas.Refund, request)

    async def _vote_in_refund(
            self,
            refund: Union[int, schemas.Refund],
            user: UserRefType,
            vote: bool
    ) -> schemas.RefundVoteResponse:
        if isinstance(user, schemas.User):
            user = user.id
        if isinstance(refund, int):
            refund = await self._get1("/v1/refunds", schemas.Refund, id=refund)
        request = schemas.VoteCreation(vote=vote, user=user, ballot_id=refund.ballot_id)
        return await self._post("/v1/refunds/vote", schemas.RefundVoteResponse, request)

    async def approve_refund(
            self,
            refund: Union[int, schemas.Refund],
            user: UserRefType
    ) -> schemas.RefundVoteResponse:
        return await self._vote_in_refund(refund, user, True)

    async def disapprove_refund(
            self,
            refund: Union[int, schemas.Refund],
            user: UserRefType
    ) -> schemas.RefundVoteResponse:
        return await self._vote_in_refund(refund, user, False)

    async def abort_refund(self, refund: Union[int, schemas.Refund], issuer: UserRefType) -> schemas.Refund:
        if isinstance(refund, schemas.Refund):
            refund = refund.id
        if isinstance(issuer, schemas.User):
            issuer = issuer.id
        request = schemas.IssuerIdBody(id=refund, issuer=issuer)
        return await self._post("/v1/refunds/abort", schemas.Refund, request)

    #####################
    # Callback handling #
    #####################

    async def create_callback(
            self,
            url: str,
            shared_secret: Optional[str] = None,
            application: Union[None, int, schemas.Application] = None
    ) -> schemas.Callback:
        application = application or self._app_id or await self.application
        if isinstance(application, schemas.Application):
            application = application.id
        callback = schemas.CallbackCreation(url=url, application_id=application, shared_secret=shared_secret)
        return await self._post("/v1/callbacks", schemas.Callback, callback)

    async def delete_callback(self, callback: Union[int, schemas.Callback]) -> bool:
        if isinstance(callback, schemas.Callback):
            callback = callback.id
        await self._delete("/v1/callbacks", data={"id": callback})
        return True

    async def delete_all_callbacks(self, application: Union[int, schemas.Application]) -> bool:
        if isinstance(application, schemas.Application):
            application = application.id
        callbacks = await self.get_callbacks(application_id=application)
        while len(callbacks) > 0:
            await self._delete("/v1/callbacks", data=callbacks.pop())
        return True
