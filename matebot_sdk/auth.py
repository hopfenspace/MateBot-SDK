"""
MateBot SDK helper middleware to easily manage proper authentication with the remote API
"""

import logging

import httpx


class APIAuthentication(httpx.Auth):
    """
    API authentication handler with automatic token renewal
    """

    requires_response_body = True

    def __init__(
            self,
            login_url: str,
            username: str,
            password: str,
            logger: logging.Logger = None,
            scope: str = "",
            client_id: str = "",
            client_secret: str = ""
    ):
        self.login_url = login_url
        self.username = username
        self.password = password
        self.logger = logger or logging.getLogger(__name__)
        self.scope = scope
        self.client_id = client_id
        self.client_secret = client_secret
        self.auth_token = ""

    def auth_flow(self, request):
        if self.auth_token:
            request.headers["Authorization"] = f"Bearer {self.auth_token}"
        response = yield request

        # Upon receiving a 401 response, a fresh login token needs to be obtained
        if response.status_code == 401:
            refresh_response = yield self.build_login_request()
            if self.update_token(refresh_response):
                request.headers["Authorization"] = f"Bearer {self.auth_token}"
                yield request

    def build_login_request(self) -> httpx.Request:
        return httpx.Request(
            "POST",
            self.login_url,
            headers={"Content-Type": "application/x-www-form-urlencoded"},
            data={
                "grant_type": "password",
                "username": self.username,
                "password": self.password,
                "scope": self.scope,
                "client_id": self.client_id,
                "client_secret": self.client_secret
            }
        )

    def update_token(self, response: httpx.Response) -> bool:
        if response.status_code != 200:
            self.logger.warning(f"Logging in with username {self.username!r} failed!")
            return False
        try:
            data = response.json()
            if "token_type" not in data or "access_token" not in data or data["token_type"].lower() != "bearer":
                self.logger.warning(f"Logging in with username {self.username!r} failed!")
                return False
            self.auth_token = data["access_token"]
            self.logger.debug("Updated login token successfully.")
            return True
        except ValueError:
            return False
        except:
            self.logger.exception("Updating login token failed!")
            raise
